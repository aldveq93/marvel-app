import MarvelContainer from './containers/MarvelContainer';
import { Navigation } from './components';
import '../node_modules/animate.css';
import './assets/scss/style.scss';

function App() {
  return (
    <section className="App">
      <Navigation />
      <MarvelContainer />
    </section>
  );
}

export default App;
