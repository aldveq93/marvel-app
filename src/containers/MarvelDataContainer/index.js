import React, { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { MarvelCard, SmallLoader } from "../../components";
import { marvelRequestDataLimit } from "../../utilities/config";
import { ThirdService } from "../../utilities/third-service";

const MarvelDataContainer = ( { urlDetail, buttonDetail, marvelDataToRender, handlerChange = null } ) => {
    const marvelService = new ThirdService();
    const offsetIncrement = marvelRequestDataLimit;
    const scrollingDataLimit = marvelDataToRender.total;
    const [scrollingData, setScrollingData] = useState([]);
    const [offset, setOffset] = useState(marvelRequestDataLimit);
    const [scrollingDataHasMore, setScrollingDataHasMore] = useState(true);

    useEffect(() => {
        setScrollingData(marvelDataToRender.data);
        return () => { setScrollingData([]) }
    }, [marvelDataToRender]);

    const getMoreScrollingData = async () => {
        let moreScrollingData = [];
        setOffset(parseInt(offset) + parseInt(offsetIncrement));

        switch (marvelDataToRender.filter) {
            case 'allCharacters':
                moreScrollingData = await marvelService.getCharactersPagination(offset);
                break;
            case 'charactersByName':
                moreScrollingData = await marvelService.getCharactersByNamePagination(marvelDataToRender.keyword, offset);
                break;
            case 'charactersByComic':
                moreScrollingData = await marvelService.getCharactersByComicPagination(marvelDataToRender.keyword, offset);
                break;
            case 'charactersByStory':
                moreScrollingData = await marvelService.getCharactersByStoryPagination(marvelDataToRender.keyword, offset);
                break;
            case 'charactersOrderedByName':
                moreScrollingData = await marvelService.getCharactersOrderedByNamePagination(offset);
                break;
            case 'allComics':
                moreScrollingData = await marvelService.getComicsPagination(offset);
                break;
            case 'comicsByFormat':
                moreScrollingData = await marvelService.getComicsByFormatPagination(marvelDataToRender.keyword, offset);
                break;
            case 'comicsByTitle':
                moreScrollingData = await marvelService.getComicsByTitlePagination(marvelDataToRender.keyword, offset);
                break;
            case 'comicsByIssueNumber':
                moreScrollingData = await marvelService.getComicsByIssueNumberPagination(marvelDataToRender.keyword, offset);
                break;
            case 'comicsOrderedByIssueNumber':
                moreScrollingData = await marvelService.getComicsOrderedByIssueNumberPagination(offset);
                break;
            case 'allStories':
                moreScrollingData = await marvelService.getStoriesPagination(offset);
                break;
            default:
                break;
        }

        if(scrollingData.length >= scrollingDataLimit) {
            setScrollingDataHasMore(false);
            return;
        }

        setScrollingData([...scrollingData, ...moreScrollingData]);
    }

    const scrollingDataWithoutHandler = scrollingData.map((scData, scDataIndex) => <MarvelCard key={`${scData.id}-${scDataIndex}`} urlDetail={urlDetail} buttonDetail={buttonDetail} marvelData={scData} />);

    const scrollingDataWithHandler = scrollingData.map((scData, scDataIndex) => <MarvelCard key={`${scData.id}-${scDataIndex}`} urlDetail={urlDetail} buttonDetail={buttonDetail} marvelData={scData} handlerDataChange={handlerChange} />);

    if (handlerChange !== null) {
        return  (
            <div className="marvel-data-to-render-container">
                {scrollingDataWithHandler}
            </div>
        );
    }

    return (
        <InfiniteScroll
            dataLength={scrollingData.length}
            next={getMoreScrollingData}
            hasMore={scrollingDataHasMore}
            loader={<SmallLoader/>}
            endMessage={
                <p className="infinite-scroll-limit-data-text">
                    <b>Yay! You have seen it all</b>
                </p>
            }
        >
            {scrollingDataWithoutHandler}
        </InfiniteScroll>
    );
}

export default MarvelDataContainer;