import React from 'react';
import Home from '../Home';
import Characters from '../Characters';
import Comics from '../Comics';
import Stories from '../Stories';
import Favorites from '../Favorites';
import SingleCharacter from '../SingleCharacter';
import SingleComic from '../SingleComic';
import SingleStory from '../SingleStory';
import { Switch, Route } from 'react-router';

const MarvelContainer = () => {
    return (
        <section className="marvel-main-container">
            <Switch>
                <Route exact path='/' component={Home} />
                <Route exact path='/characters' component={Characters} />
                <Route exact path='/characters/:id' component={SingleCharacter} />
                <Route exact path='/comics' component={Comics} />
                <Route exact path='/comics/:id' component={SingleComic} />
                <Route exact path='/stories' component={Stories} />
                <Route exact path='/stories/:id' component={SingleStory} />
                <Route exact path='/favorites' component={Favorites} />
            </Switch>
        </section>
    );
}

export default MarvelContainer;