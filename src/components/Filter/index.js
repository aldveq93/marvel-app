import React, { useState } from 'react';
import { ThirdService } from '../../utilities/third-service';

const Filter = ({ updateMarvelData, resetMarvelData, setLoading, setError, marvelFilterType }) => {

    const marvelService = new ThirdService();

    const [filterType, setFilterType] = useState({
        filterName: '',
        searchKeyword: ''
    });
    const [isComicsFormatSelectEnable, setIsComicsFormatSelectEnable] = useState(false);
    const [isComicsIssueNumberInputEnable, setIsComicsIssueNumberInputEnable] = useState(false);
    const [isDataOrderedByAlphabet, setIsDataOrderedByAlphabet] = useState(true);

    const setFilter = e => {

        setFilterType({
            ...filterType,
            [e.target.name]: e.target.value
        })
        
        if (e.target.value === 'format') {
            setIsComicsFormatSelectEnable(true);
            setIsComicsIssueNumberInputEnable(false);

            setFilterType({
                filterName: e.target.value,
                searchKeyword: ''
            });

        } else if (e.target.value === 'issueNumber') {
            setIsComicsIssueNumberInputEnable(true); 
            setIsComicsFormatSelectEnable(false);

            setFilterType({
                filterName: e.target.value,
                searchKeyword: ''
            });

        } else if (e.target.value === 'title') {
            setIsComicsFormatSelectEnable(false);
            setIsComicsIssueNumberInputEnable(false);    
            
            setFilterType({
                filterName: e.target.value,
                searchKeyword: ''
            });

        }
    }

    const getMarvelFilterOptions = () => {

        if(marvelFilterType === 'characters')
            return(
                <>
                    <option value="name">Name</option>
                    <option value="comics">Comics</option>
                    <option value="stories">Stories</option>
                </>
            );

        return(
                <>
                    <option value="format">Format</option>
                    <option value="title">Title</option>
                    <option value="issueNumber">Issue number</option>
                </>
        );
    }

    const filterMarvelData = e => {

        e.stopPropagation();

        const filterTypeText = filterType.filterName;
        const filterKeywordText = filterType.searchKeyword; 

        if ( filterTypeText === '' && filterKeywordText === '' ) {
            alert('Please, provide a filter type and a search term!');
            return;
        }

        if ( filterTypeText === '' ) {
            alert('Please, provide a filter type!');
            return;
        }

        if ( filterKeywordText === '' ) {
            alert('Please, provide a search term!');
            return;
        }

        filterMarvelDataByFilterType(filterTypeText, filterKeywordText);

    }

    const filterMarvelDataByFilterType = (filterTypeText, filterKeywordText) => {

        if ( filterTypeText === 'name') {
            filterCharacterByName(filterKeywordText);
            return;
        }

        if ( filterTypeText === 'comics') {
            filterCharacterByComics(filterKeywordText);
            return;
        }

        if ( filterTypeText === 'stories') {
            filterCharacterByStories(filterKeywordText);
            return;
        }

        if ( filterTypeText === 'format') {
            filterComicsByFormat(filterKeywordText);
            return;
        }

        if ( filterTypeText === 'title') {
            filterComicsByTitle(filterKeywordText);
            return;
        }

        if ( filterTypeText === 'issueNumber') {
            filterComicsByIssueNumber(filterKeywordText);
            return;
        }
    };

    const filterCharacterByName = async (keyword) => {
        setLoading(true);
        const charactersByName = await marvelService.getCharactersByName(keyword);

        if(charactersByName.error) {
            const { message } = charactersByName.error;
            setLoading(false);
            setError({active: true, message: `Oh, there was an error on your search characters by name request: ${message}!`});
            return;
        }

        if (charactersByName.data.length <= 0) {
            setLoading(false);
            setError({active: true, message: `We are sorry, we did not find a character by the search term: "${keyword}"!`});
            return;
        }

        updateMarvelData({
            total: charactersByName.total,
            filter: 'charactersByName',
            keyword,
            data: charactersByName.data
        });

        setFilterType({
            filterName: '',
            searchKeyword: ''
        });
        setLoading(false);
    }

    const cleanFilter = e => {
        e.stopPropagation();

        setFilterType({
            filterName: '',
            searchKeyword: ''
        });

        resetMarvelData(); 
    }

    const filterCharacterByComics = async (keyword) => {
        setLoading(true);
        const marvelComics = await marvelService.getComicsByTitle(keyword);        

        if(marvelComics.error) {
            const { message } = marvelComics.error;
            setLoading(false);
            setError({active: true, message: `Oh, there was an error on your search characters by comic request: ${message}!`});
            return;
        }

        const comicsIds = getArrayIds(marvelComics.data);
        const comicsIdsText = comicsIds.join(','); 
        
        if (comicsIdsText === '') {
            setLoading(false);
            setError({active: true, message: `We are sorry, we did not find characters by the comic: "${keyword}"!`});
            return;
        }

        const characterByComics = await marvelService.getCharacterByComics(comicsIdsText);

        if(characterByComics.error) {
            const { message } = characterByComics.error;
            setLoading(false);
            setError({active: true, message: `Oh, there was an error on your search characters by comic 2 request: ${message}!`});
            return;
        }

        if(characterByComics.data.length <= 0) {
            setLoading(false);
            setError({active: true, message: `We are sorry, we did not find characters by the comic: "${keyword}"!`});
            return;
        }

        updateMarvelData({
            total: characterByComics.total,
            filter: 'charactersByComic',
            keyword: comicsIdsText,
            data: characterByComics.data
        });

        setFilterType({
            filterName: '',
            searchKeyword: ''
        });
        setLoading(false);
    }
    
    const filterCharacterByStories = async (keyword) => {
        setLoading(true);
        const marvelStories = await marvelService.getStories();

        if(marvelStories.error) {
            const { message } = marvelStories.error;
            setLoading(false);
            setError({active: true, message: `Oh, there was an error on your search characters by story request: ${message}!`});
            return;
        }

        const filteredMarvelStories = marvelStories.data.filter(ms => ((ms.title).toLowerCase()).indexOf((keyword).toLowerCase()) !== -1 );
        const marvelStoriesIds = getArrayIds(filteredMarvelStories);
        const marvelStoriesIdsText = marvelStoriesIds.join(',');

        if (marvelStoriesIdsText === '') {
            setLoading(false);
            setError({active: true, message: `We are sorry, we did not find characters by the story: "${keyword}"!`});
            return;
        }

        const characterByStories = await marvelService.getCharacterByStories(marvelStoriesIdsText);

        if(characterByStories.error) {
            const { message } = characterByStories.error;
            setLoading(false);
            setError({active: true, message: `Oh, there was an error on your search characters by story request 2: ${message}!`});
            return;
        }

        if(characterByStories.data.length === 0) {
            setLoading(false);
            setError({active: true, message: `We are sorry, we did not find characters by the story: "${keyword}"!`});
            return;
        }

        updateMarvelData({
            total: characterByStories.total,
            filter: 'charactersByStory',
            keyword: marvelStoriesIdsText,
            data: characterByStories.data
        });

        setFilterType({
            filterName: '',
            searchKeyword: ''
        });
        setLoading(false);
    }

    const filterComicsByFormat = async (keyword) => {
        setLoading(true);
        const marvelComicsByFormat = await marvelService.getComicsByFormat(keyword); 

        if(marvelComicsByFormat.error) {
            const { message } = marvelComicsByFormat.error;
            setLoading(false);
            setError({active: true, message: `Oh, there was an error on your comics by format request: ${message}!`});
            return;
        }

        if(marvelComicsByFormat.data.length === 0) {
            setLoading(false);
            setError({active: true, message: `We are sorry, we did not find comics by format: "${keyword}"!`});
            return;
        }

        updateMarvelData({
            total: marvelComicsByFormat.total,
            filter: 'comicsByFormat',
            keyword,
            data: marvelComicsByFormat.data
        });

        setFilterType({
            filterName: '',
            searchKeyword: ''
        });
        setLoading(false);
    }

    const filterComicsByTitle = async (keyword) => {
        setLoading(true);
        const marvelComicsByTitle = await marvelService.getComicsByTitle(keyword);

        
        if(marvelComicsByTitle.error) {
            const { message } = marvelComicsByTitle.error;
            setLoading(false);
            setError({active: true, message: `Oh, there was an error on your comics by title request: ${message}!`});
            return;
        }

        if(marvelComicsByTitle.data.length === 0) {
            setLoading(false);
            setError({active: true, message: `We are sorry, we did not find comics by the title: "${keyword}"!`});
            return;
        }

        updateMarvelData({
            total: marvelComicsByTitle.total,
            filter: 'comicsByTitle',
            keyword,
            data: marvelComicsByTitle.data
        });

        setFilterType({
            filterName: '',
            searchKeyword: ''
        });
        setLoading(false);
    }

    const filterComicsByIssueNumber = async (keyword) => {
        setLoading(true);
        const marvelComicsByIssueNumber = await marvelService.getComicsByIssueNumber(keyword);

        if(marvelComicsByIssueNumber.error) {
            const { message } = marvelComicsByIssueNumber.error;
            setLoading(false);
            setError({active: true, message: `Oh, there was an error on your comics by issue number request: ${message}!`});
            return;
        }

        if(marvelComicsByIssueNumber.data.length === 0) {
            setLoading(false);
            setError({active: true, message: `We are sorry, we did not find comics by issue number: "${keyword}"!`});
            return;
        }

        updateMarvelData({
            total: marvelComicsByIssueNumber.total,
            filter: 'comicsByIssueNumber',
            keyword,
            data: marvelComicsByIssueNumber.data
        });

        setFilterType({
            filterName: '',
            searchKeyword: ''
        });
        setLoading(false);
    }

    const getSearchTermInput = () => {
        if (isComicsFormatSelectEnable)
            return (
                <select className="filter-container__select-search-type" name="searchKeyword" value={filterType.searchKeyword} onChange={ setFilter }>
                    <option value="">Select format</option>
                    <option value="Comic">Comic</option>
                    <option value="Magazine">Magazine</option>
                    <option value="Trade paperback">Trade paperback</option>
                    <option value="Hardcover">Hardcover</option>
                    <option value="Digest">Digest</option>
                    <option value="Graphic novel">Graphic novel</option>
                    <option value="Digital comic">Digital comic</option>
                    <option value="Infinite comic">Infinite comic</option>
                </select>
            )

        if (isComicsIssueNumberInputEnable) 
            return (
                <input type="number" placeholder="Type your issue number" className="filter-container__input-search" name="searchKeyword" value={filterType.searchKeyword} onChange={ setFilter } />                
            )

        return  (
            <input type="text" placeholder="Type your search term" className="filter-container__input-search" name="searchKeyword" value={filterType.searchKeyword} onChange={ setFilter } />
        )
    }

    const orderMarvelDataByName = async () => {
        setIsDataOrderedByAlphabet(!isDataOrderedByAlphabet);
        setLoading(true);
        const marvelCharactersDataOrderedByName = await marvelService.getCharactersOrderedByName();

        if(marvelCharactersDataOrderedByName.error) {
            const { message } = marvelCharactersDataOrderedByName.error;
            setLoading(false);
            setError({active: true, message: `Oh, there was an error on your order characters by name request: ${message}!`});
            return;
        }

        if(marvelCharactersDataOrderedByName.data.length === 0) {
            setLoading(false);
            setError({active: true, message: 'We are sorry, we could not order the characters by name'});
            return;
        }

        if(!isDataOrderedByAlphabet) {
            const marvelCharactersResetData = await marvelService.getCharacters();
            updateMarvelData({
                total: marvelCharactersResetData.total,
                filter: 'allCharacters',
                keyword: '',
                data: marvelCharactersResetData.data
            });        
            setLoading(false);    
            return;
        }

        updateMarvelData({
            total: marvelCharactersDataOrderedByName.total,
            filter: 'charactersOrderedByName',
            keyword: '',
            data: marvelCharactersDataOrderedByName.data
        });
        setLoading(false);    
    }

    const orderMarvelDataByIssueNumber = async () => {
        setIsDataOrderedByAlphabet(!isDataOrderedByAlphabet);
        setLoading(true);
        const marvelComicsDataOrderedByIssueNumber = await marvelService.getComicsOrderedByIssueNumber();

        if(marvelComicsDataOrderedByIssueNumber.error) {
            const { message } = marvelComicsDataOrderedByIssueNumber.error;
            setLoading(false);
            setError({active: true, message: `Oh, there was an error on your order comics by issue number request: ${message}!`});
            return;
        }

        if(marvelComicsDataOrderedByIssueNumber.data.length === 0) {
            setLoading(false);
            setError({active: true, message: 'We are sorry, we could not order the comics by issue number'});
            return;
        }

        if(!isDataOrderedByAlphabet) {
            const marvelComicsResetData = await marvelService.getComics();
            updateMarvelData({
                total: marvelComicsResetData.total,
                filter: 'allComics',
                keyword: '',
                data: marvelComicsResetData.data
            });        
            setLoading(false);    
            return;
        }
        
        updateMarvelData({
            total: marvelComicsDataOrderedByIssueNumber.total,
            filter: 'comicsOrderedByIssueNumber',
            keyword: '',
            data: marvelComicsDataOrderedByIssueNumber.data
        });
        setLoading(false);
    }

    const drawOrderButton = () => {
        if(marvelFilterType === 'characters')
            return <button className="filter-container__filter-button" onClick={ orderMarvelDataByName }>Order by Name</button>

        return (
            <button className="filter-container__filter-button" onClick={ orderMarvelDataByIssueNumber }>Order by Issue Number</button>
        );  
    }

    const getArrayIds = dataArray => {
        const idsArray = dataArray.map((c) => c.id);

        if(idsArray.length > 10) {
            return idsArray.slice(0, 10);
        }

        return idsArray;
    }

    return (
        <div className="filter-container animate__animated animate__fadeIn animate__delay-1s">
            <span className="filter-container__title">Filter by:</span>
            <select className="filter-container__select-search-type" name="filterName" value={filterType.filterName} onChange={ setFilter }>
                <option value="">Choose an option</option>
                {getMarvelFilterOptions()}
            </select>
            {getSearchTermInput()}
            <button className="filter-container__filter-button" onClick={ filterMarvelData }>Filter</button>
            <button className="filter-container__filter-button" onClick={ cleanFilter }>Show All</button>
            {drawOrderButton()}
        </div>
    );
};

export default Filter;