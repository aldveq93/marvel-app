import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import logo from '../../assets/images/marvel.svg';

const Navigation = () => {

    const [isActive, setIsActive] = useState(false);

    const handlerHamburger = () => {
        isActive ? setIsActive(false) : setIsActive(true);
    }

    const onNavItemClick = (value) => () => {
        setIsActive(value);
    }

    return(
        <>
            <button className={`hamburger hamburger--collapse ${isActive && 'is-active'}`} type="button" onClick={ handlerHamburger }>
                <span className="hamburger-box" onClick={ handlerHamburger }>
                    <span className="hamburger-inner" onClick={ handlerHamburger }></span>
                </span>
            </button>
            <nav className={`main-navigation ${isActive && 'show'}`}>
                <ul className="main-navigation__wrapper">
                    <li className="main-navigation__item" onClick={onNavItemClick(false)}>
                        <Link to="/">Home</Link>
                    </li>
                    <li className="main-navigation__item" onClick={onNavItemClick(false)}>
                        <Link to="/characters">Characters</Link>
                    </li>
                    <li className="main-navigation__item" onClick={onNavItemClick(false)}>
                        <Link to="/comics">Comics</Link>
                    </li>
                    <li className="main-navigation__item" onClick={onNavItemClick(false)}>
                        <Link to="/stories">Stories</Link>
                    </li>
                    <li className="main-navigation__item" onClick={onNavItemClick(false)}>
                        <Link to="/favorites">Favorites</Link>
                    </li>
                </ul>
                <img src={logo} className="main-navigation__logo" alt="Marvel App" />
            </nav>
        </>
    );
}

export default Navigation;