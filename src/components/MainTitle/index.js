import React from 'react';

const MainTitle = ({title}) => {
    return(
        <h1 className="marvel-main-container__title animate__animated animate__fadeIn animate__delay-1s">{title}</h1>
    );
}

export default MainTitle;