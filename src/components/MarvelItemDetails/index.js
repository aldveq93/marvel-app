import React from 'react'
import TitleList from '../TitleList';
import { getMarvelTitleOrName, getMarvelImage } from '../../utilities/utilities';

const MarvelItemDetails = ({ marvelDataDetails, listOne, listTwo, titleListOne, titleListTwo, urlListOne, urlListTwo }) => {

    const marvelItemName = getMarvelTitleOrName(marvelDataDetails);
    const marvelItemImage = getMarvelImage(marvelDataDetails);

    return (
        <div className="single-caracther-container animate__animated animate__fadeIn animate__delay-1s">
            <img src={`${marvelItemImage}`} alt={marvelItemName} className="single-caracther-container__image" />
            <div className="single-caracther-container__info-container">
                <h3 className="single-caracther-container__name">{marvelItemName}</h3>
                <p className="single-caracther-container__description">{marvelDataDetails?.description !== '' ? marvelDataDetails?.description : 'Description not available :('}</p>
                <TitleList title={titleListOne} data={listOne} url={urlListOne}/>
                <TitleList title={titleListTwo} data={listTwo} url={urlListTwo}/>
            </div>
        </div>
    )
}

export default MarvelItemDetails;
